//
//  ViewController.swift
//  StickerVideoAR
//
//  Created by Emre on 26.12.2020.
//

import UIKit
import ARKit

//MARK: AAA
// You don't have to use like a different page. You can add this methods on your video page controller

//MARK: Variables
private let planeWidth: CGFloat = 0.13
private let planeHeight: CGFloat = 0.06
private let nodeYPosition: Float = 0.024
private let minPositionDistance: Float = 0.0025
private let minScaling: CGFloat = 0.025
private let cellIdentifier = "CollectionViewCell"
private let glassesCount = 6
private let animationDuration: TimeInterval = 0.25
private let cornerRadius: CGFloat = 10

class ViewController: UIViewController {
    
    private let glassesPlane = SCNPlane(width: planeWidth, height: planeHeight)
    private let glassesNode = SCNNode()
    
    private let nosePlane = SCNPlane(width: 0.08, height: 0.04)
    private let noseNode = SCNNode()
    
    var isOpen = false
    var count: Int = 0
    
    // This collectionView just for sticker package
    let collectionView : UICollectionView = {
       
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(CollectionCell.self, forCellWithReuseIdentifier: cellIdentifier)
        return cv
        
    }()

    // ARView for ARKit and face detection
    let arView: ARSCNView = {
       
        let arV = ARSCNView()
        arV.backgroundColor = .clear
        return arV
    }()
    
    //This line set visibility collectionView
    lazy var collectionButton: UIImageView = {
       
        let collectionBtn = UIImageView()
        collectionBtn.contentMode = .scaleAspectFit
        collectionBtn.image = #imageLiteral(resourceName: "icons8-labels-64")
        collectionBtn.clipsToBounds = true
        collectionBtn.frame.size = CGSize(width: 30, height: 30)
        collectionBtn.isUserInteractionEnabled = true
        
        let tappedImage = UITapGestureRecognizer(target: self, action: #selector(handleCollection))
        collectionBtn.addGestureRecognizer(tappedImage)
        return collectionBtn
    }()


    @objc func handleCollection() {
    
        let configuration = ARFaceTrackingConfiguration()
        arView.session.run(configuration)
        
        let top = CGAffineTransform(translationX: 0, y: -110)
        let bottom = CGAffineTransform(translationX: 0, y: 10)
        
        if isOpen == false {
            
        UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
        
              self.collectionButton.transform = top
              self.collectionView.alpha = 1.0
              self.isOpen = true
            
        }, completion: nil)
            
        } else {
            
            UIView.animate(withDuration: 0.4, delay: 0.0, options: [], animations: {
            
                  self.collectionButton.transform = bottom
                  self.collectionView.alpha = 0.0
                  self.isOpen = false
            }, completion: nil)
        }
    }
    
    
    let audioButton: UIButton = {
       
        let audioBtn = UIButton()
        audioBtn.setImage(#imageLiteral(resourceName: "icons8-music-60"), for: .normal)
        audioBtn.addTarget(self, action: #selector(handleAudio), for: .touchUpInside)
        return audioBtn
    }()
    
    @objc func handleAudio() {
        
        print("Tapped Audio")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .brown
        setAnchors()
    
        guard ARFaceTrackingConfiguration.isSupported else { fatalError() }
        arView.delegate = self
    }
    
    fileprivate func setAnchors() {
        
        view.addSubview(arView)
        arView.translatesAutoresizingMaskIntoConstraints = false
        arView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0.0).isActive = true
        arView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0.0).isActive = true
        arView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0.0).isActive = true
        arView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0.0).isActive = true
        
        view.addSubview(collectionView)
        collectionView.backgroundColor = .clear
        collectionView.layer.cornerRadius = 12.0
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0.0).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10.0).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10.0).isActive = true
        collectionView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        collectionView.alpha = 0
        
        view.addSubview(collectionButton)
        collectionButton.translatesAutoresizingMaskIntoConstraints = false
        collectionButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 12.0).isActive = true
        collectionButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -12.0).isActive = true
        collectionButton.widthAnchor.constraint(equalToConstant: 100).isActive = true
        collectionButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        view.addSubview(audioButton)
        audioButton.translatesAutoresizingMaskIntoConstraints = false
        audioButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0.0).isActive = true
        audioButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12.0).isActive = true
        audioButton.heightAnchor.constraint(equalToConstant: 44).isActive = true
        audioButton.widthAnchor.constraint(equalToConstant: 44).isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        let configuration = ARFaceTrackingConfiguration()
//        arView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        arView.session.pause()
    }
}

extension ViewController: ARSCNViewDelegate {
    
    //This line how we nailed face mask and positions
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        guard let device = arView.device else {
            return nil
        }

        let faceGeometry = ARSCNFaceGeometry(device: device)
        let faceNode = SCNNode(geometry: faceGeometry)
        faceNode.geometry?.firstMaterial?.transparency = 0

        glassesPlane.firstMaterial?.isDoubleSided = true
        updateGlasses(with: 0)

        glassesNode.position.z = faceNode.boundingBox.max.z * 3 / 4
        glassesNode.position.y = nodeYPosition
        glassesNode.geometry = glassesPlane

        faceNode.addChildNode(glassesNode)

        nosePlane.firstMaterial?.isDoubleSided = true
        updateNose(with: 0)
        
        noseNode.position.z = faceNode.boundingBox.max.z * 3 / 4
        noseNode.position.y = -0.010
        noseNode.geometry = nosePlane
        
        faceNode.addChildNode(noseNode)


        return faceNode
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let faceAnchor = anchor as? ARFaceAnchor, let faceGeometry = node.geometry as? ARSCNFaceGeometry else {
            return
        }

        faceGeometry.update(from: faceAnchor.geometry)
    }
    
    private func updateGlasses(with index: Int) {
        let imageName = "glasses\(index)"
        glassesPlane.firstMaterial?.diffuse.contents = UIImage(named: imageName)
    }
    
    private func updateNose(with index: Int) {
        let imageName = "nose\(index)"
        nosePlane.firstMaterial?.diffuse.contents = UIImage(named: imageName)
    }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return glassesCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! CollectionCell
        let imageName = "glasses\(indexPath.row)"
        let cornerRadius: CGFloat = 80 / 2
        
        cell.setup(with: imageName)
        cell.layer.cornerRadius = cornerRadius
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        updateGlasses(with: indexPath.row)
        updateNose(with: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = 80
        let width = 80
        
        return CGSize(width: width, height: height)
    }
}

