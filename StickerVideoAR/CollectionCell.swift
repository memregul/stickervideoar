//
//  CollectionCell.swift
//  StickerVideoAR
//
//  Created by Emre on 26.12.2020.
//

import UIKit

class CollectionCell: UICollectionViewCell {
    
    let cellImageView: UIImageView = {
       
        let cellIV = UIImageView()
        cellIV.contentMode = .scaleAspectFit
        cellIV.clipsToBounds = true
        cellIV.backgroundColor = .clear
        return cellIV
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.6)
        setCellAnchors()
    }
    
    fileprivate func setCellAnchors() {
        
        addSubview(cellImageView)
        cellImageView.translatesAutoresizingMaskIntoConstraints = false
        cellImageView.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        cellImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20).isActive = true
        cellImageView.leftAnchor.constraint(equalTo: leftAnchor,constant: 12.0).isActive = true
        cellImageView.rightAnchor.constraint(equalTo: rightAnchor, constant: -12.0).isActive = true
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setup(with imageName: String) {
        cellImageView.image = UIImage(named: imageName)
    }
    
}
